## Redux Demo

### Steps

#### 1. Create the store
#### 2. Provide the store
#### 3. Reducer logic
#### 4. Select state
#### 5. Dispatch actions
